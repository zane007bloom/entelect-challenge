package za.co.entelect.challenge.enums;

public enum CommandType {
    BUILD_DEFENSE("0"),
    BUILD_ATTACK("1"),
    BUILD_ENERGY("2"),
    DECONSTRUCT("3"),
    BUILD_TESLA("4"),
    ACTIVATE_IRON_CURTAIN("5");

    private final String commandCode;

    CommandType(String commandCode) {
        this.commandCode = commandCode;
    }

    public String getCommandCode() {
        return commandCode;
    }
}
