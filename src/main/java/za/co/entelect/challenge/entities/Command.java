package za.co.entelect.challenge.entities;

import za.co.entelect.challenge.enums.CommandType;
import za.co.entelect.challenge.enums.PlayerType;

import java.util.Objects;

public class Command {

    private int x;
    private int y;
    private CommandType commandType;
    private PlayerType playerType;

    public Command(int x, int y, CommandType commandType, PlayerType playerType) {
        this.x = x;
        this.y = y;
        this.commandType = commandType;
        this.playerType = playerType;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public CommandType getCommandType() {
        return commandType;
    }

    public PlayerType getPlayerType() {
        return playerType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Command command = (Command) o;
        return x == command.x &&
                y == command.y &&
                commandType == command.commandType &&
                playerType == command.playerType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, commandType, playerType);
    }
}
