package za.co.entelect.challenge.entities;

import za.co.entelect.challenge.enums.CommandType;

import java.util.Map;

public class GameDetails {

    private int round;
    private int mapWidth;
    private int mapHeight;
    private int roundIncomeEnergy;
    private Map<CommandType, Building> buildingsStats;
    private IronCurtainStats ironCurtainStats;

    public GameDetails(GameDetails gameDetails) {
        round = gameDetails.round;
        mapWidth = gameDetails.mapWidth;
        mapHeight = gameDetails.mapHeight;
        roundIncomeEnergy = gameDetails.roundIncomeEnergy;
        buildingsStats = gameDetails.buildingsStats;
        ironCurtainStats = gameDetails.ironCurtainStats;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public int getMapWidth() {
        return mapWidth;
    }

    public void setMapWidth(int mapWidth) {
        this.mapWidth = mapWidth;
    }

    public int getMapHeight() {
        return mapHeight;
    }

    public void setMapHeight(int mapHeight) {
        this.mapHeight = mapHeight;
    }

    public int getRoundIncomeEnergy() {
        return roundIncomeEnergy;
    }

    public void setRoundIncomeEnergy(int roundIncomeEnergy) {
        this.roundIncomeEnergy = roundIncomeEnergy;
    }

    public Map<CommandType, Building> getBuildingsStats() {
        return buildingsStats;
    }

    public void setBuildingsStats(Map<CommandType, Building> buildingsStats) {
        this.buildingsStats = buildingsStats;
    }

    public IronCurtainStats getIronCurtainStats() {
        return ironCurtainStats;
    }

    public void setIronCurtainStats(IronCurtainStats ironCurtainStats) {
        this.ironCurtainStats = ironCurtainStats;
    }

    @Override
    public GameDetails clone() {
        return new GameDetails(this);
    }
}

