package za.co.entelect.challenge.mcts;

import za.co.entelect.challenge.entities.Command;
import za.co.entelect.challenge.entities.GameState;
import za.co.entelect.challenge.enums.PlayerType;

import java.util.HashMap;
import java.util.Map;

public class Node {

    private GameState gameState;
    private Node parent;
    private Map<Command, Node> children = new HashMap<>();

    private int numberOfPlays;
    private int numberOfWins;

    private PlayerType playerType;

    public Node(Node parent, GameState gameState, PlayerType playerType) {
        this.parent = parent;
        this.gameState = gameState;
        this.playerType = playerType;
    }

    public Node(GameState gameState, PlayerType playerType) {
        this.gameState = gameState;
        this.playerType = playerType;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public Map<Command, Node> getChildren() {
        return children;
    }

    public int getNumberOfPlays() {
        return numberOfPlays;
    }

    public void setNumberOfPlays(int numberOfPlays) {
        this.numberOfPlays = numberOfPlays;
    }

    public int getNumberOfWins() {
        return numberOfWins;
    }

    public void setNumberOfWins(int numberOfWins) {
        this.numberOfWins = numberOfWins;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public void addChild(Command command, Node child) {
        children.put(command, child);
    }

    public void increaseNumberOfPlays() {
        numberOfPlays++;
    }

    public void increaseNumberOfWins() {
        numberOfWins++;
    }

    public boolean hasParent() {
        return parent != null;
    }

    public float getWinAverage() {
        return numberOfWins / (float) numberOfPlays;
    }

    public boolean isTerminal() {
        return gameState.isGameOver();
    }

    public boolean isFullyExpanded() {
        return children.size() == gameState.numberOfPossibleMoves(playerType);
    }

    public PlayerType getPlayerType() {
        return playerType;
    }
}
