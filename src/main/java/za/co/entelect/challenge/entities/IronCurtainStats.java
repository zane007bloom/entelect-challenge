package za.co.entelect.challenge.entities;

public class IronCurtainStats {

    private int activeRounds;
    private int resetPeriod;
    private int price;
    private int constructionScore;

    public int getActiveRounds() {
        return activeRounds;
    }

    public void setActiveRounds(int activeRounds) {
        this.activeRounds = activeRounds;
    }

    public int getResetPeriod() {
        return resetPeriod;
    }

    public void setResetPeriod(int resetPeriod) {
        this.resetPeriod = resetPeriod;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getConstructionScore() {
        return constructionScore;
    }

    public void setConstructionScore(int constructionScore) {
        this.constructionScore = constructionScore;
    }
}
