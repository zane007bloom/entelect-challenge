package za.co.entelect.challenge.entities;

import java.util.Objects;

public class Building {

    private int health;
    private int constructionTime;
    private int price;
    private int weaponDamage;
    private int weaponSpeed;
    private int weaponCooldownPeriod;
    private int energyGeneratedPerTurn;
    private int destroyMultiplier;
    private int constructionScore;

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getConstructionTime() {
        return constructionTime;
    }

    public void setConstructionTime(int constructionTime) {
        this.constructionTime = constructionTime;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getWeaponDamage() {
        return weaponDamage;
    }

    public void setWeaponDamage(int weaponDamage) {
        this.weaponDamage = weaponDamage;
    }

    public int getWeaponSpeed() {
        return weaponSpeed;
    }

    public void setWeaponSpeed(int weaponSpeed) {
        this.weaponSpeed = weaponSpeed;
    }

    public int getWeaponCooldownPeriod() {
        return weaponCooldownPeriod;
    }

    public void setWeaponCooldownPeriod(int weaponCooldownPeriod) {
        this.weaponCooldownPeriod = weaponCooldownPeriod;
    }

    public int getEnergyGeneratedPerTurn() {
        return energyGeneratedPerTurn;
    }

    public void setEnergyGeneratedPerTurn(int energyGeneratedPerTurn) {
        this.energyGeneratedPerTurn = energyGeneratedPerTurn;
    }

    public int getDestroyMultiplier() {
        return destroyMultiplier;
    }

    public void setDestroyMultiplier(int destroyMultiplier) {
        this.destroyMultiplier = destroyMultiplier;
    }

    public int getConstructionScore() {
        return constructionScore;
    }

    public void setConstructionScore(int constructionScore) {
        this.constructionScore = constructionScore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Building building = (Building) o;
        return health == building.health &&
                constructionTime == building.constructionTime &&
                price == building.price &&
                weaponDamage == building.weaponDamage &&
                weaponSpeed == building.weaponSpeed &&
                weaponCooldownPeriod == building.weaponCooldownPeriod &&
                energyGeneratedPerTurn == building.energyGeneratedPerTurn &&
                destroyMultiplier == building.destroyMultiplier &&
                constructionScore == building.constructionScore;
    }

    @Override
    public int hashCode() {

        return Objects.hash(health, constructionTime, price, weaponDamage, weaponSpeed, weaponCooldownPeriod, energyGeneratedPerTurn, destroyMultiplier, constructionScore);
    }
}
