package za.co.entelect.challenge.entities;

import za.co.entelect.challenge.enums.CommandType;
import za.co.entelect.challenge.enums.PlayerType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class RandomCommandGenerator {

    public Command generate(GameState gameState, PlayerType playerType, Collection<Command> commandsToIgnore) {
        int energy = gameState.getPlayer(playerType).getEnergy();

        List<CellStateContainer> validCellsToBuildOn = gameState.getEmptyCells(playerType);

        List<CommandType> commandTypes = generateValidCommandTypes(gameState, energy, validCellsToBuildOn);

        Command command;
        do {
            CommandType commandType = chooseRandom(commandTypes);
            CellStateContainer cell = chooseRandom(validCellsToBuildOn);
            command = new Command(cell.getX(), cell.getY(), commandType, playerType);
        } while (commandsToIgnore.contains(command));

        return command;
    }

    private <T> T chooseRandom(List<T> elements) {
        Random random = new Random();
        int randomIndex = random.nextInt(elements.size());
        return elements.get(randomIndex);
    }

    private List<CommandType> generateValidCommandTypes(GameState gameState, int energy, List<CellStateContainer> emptyCells) {
        List<CommandType> commandTypes = new ArrayList<>();
        if (!emptyCells.isEmpty()) {
            if (energy >= gameState.getGameDetails().getBuildingsStats().get(CommandType.BUILD_ENERGY).getPrice()) {
                commandTypes.add(CommandType.BUILD_ENERGY);
            }
            if (energy >= gameState.getGameDetails().getBuildingsStats().get(CommandType.BUILD_DEFENSE).getPrice()) {
                commandTypes.add(CommandType.BUILD_DEFENSE);
            }
            if (energy >= gameState.getGameDetails().getBuildingsStats().get(CommandType.BUILD_ATTACK).getPrice()) {
                commandTypes.add(CommandType.BUILD_ATTACK);
            }
        }
        return commandTypes;
    }

}
