package za.co.entelect.challenge.entities;

import za.co.entelect.challenge.enums.PlayerType;

import java.util.ArrayList;
import java.util.List;

public class CellStateContainer {
    private int x;
    private int y;
    private PlayerType cellOwner;
    private List<Building> buildings;
    private List<Missile> missiles;

    public CellStateContainer(int x, int y, PlayerType cellOwner) {
        this.x = x;
        this.y = y;
        this.cellOwner = cellOwner;
        this.buildings = new ArrayList<>();
        this.missiles = new ArrayList<>();
    }

    public CellStateContainer(CellStateContainer cellStateContainer) {
        x = cellStateContainer.x;
        y = cellStateContainer.y;
        cellOwner = cellStateContainer.cellOwner;
        buildings = new ArrayList<>(cellStateContainer.buildings);
        missiles = new ArrayList<>(cellStateContainer.missiles);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public PlayerType getCellOwner() {
        return cellOwner;
    }

    public void setCellOwner(PlayerType cellOwner) {
        this.cellOwner = cellOwner;
    }

    public List<Building> getBuildings() {
        return buildings;
    }

    public void setBuildings(List<Building> buildings) {
        this.buildings = buildings;
    }

    public List<Missile> getMissiles() {
        return missiles;
    }

    public void setMissiles(List<Missile> missiles) {
        this.missiles = missiles;
    }

    public void addBuilding(Building building, int round) {
        buildings.add(building);
    }

    public void addMissiles(Missile missile) {
        missiles.add(missile);
    }

    @Override
    public CellStateContainer clone() {
        return new CellStateContainer(this);
    }
}
