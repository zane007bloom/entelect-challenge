package za.co.entelect.challenge.entities;

import za.co.entelect.challenge.enums.CommandType;
import za.co.entelect.challenge.enums.PlayerType;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

public class GameState {

    private static final int MAX_ROUNDS = 400;

    private GameDetails gameDetails;
    private List<Player> players;
    private CellStateContainer[][] gameMap;

    public GameState(GameState gameState) {
        gameDetails = gameState.gameDetails.clone();
        players = gameState.players;
        gameMap = cloneGameMap();
    }

    private CellStateContainer[][] cloneGameMap() {
        int mapHeight = gameDetails.getMapHeight();
        int mapWidth = gameDetails.getMapWidth();
        CellStateContainer[][] newCells = new CellStateContainer[mapHeight][mapWidth];
        for (int i = 0; i < mapWidth; i++) {
            for (int j = 0; j < mapHeight; j++) {
                newCells[j][i] = gameMap[j][i].clone();
            }
        }
        return newCells;
    }

    public GameDetails getGameDetails() {
        return gameDetails;
    }

    public void setGameDetails(GameDetails gameDetails) {
        this.gameDetails = gameDetails;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public Player getPlayer(PlayerType playerType) {
        return players.stream().filter(player -> player.getPlayerType() == playerType).findFirst().get();
    }

    public CellStateContainer[][] getGameMap() {
        return gameMap;
    }

    public void setGameMap(CellStateContainer[][] gameMap) {
        this.gameMap = gameMap;
    }

    public List<CellStateContainer> getFlattenedGameMap() {
        List<CellStateContainer> allCellStates = new ArrayList<>();

        for (CellStateContainer[] column : gameMap) {
            allCellStates.addAll(asList(column));
        }

        return allCellStates;
    }

    public long getNumberOfEmptyCells(PlayerType playerType) {
        return getFlattenedGameMap().stream()
                .filter(cell -> cell.getCellOwner() == playerType && cell.getBuildings().isEmpty()).count();
    }

    public List<CellStateContainer> getEmptyCells(PlayerType playerType) {
        return getFlattenedGameMap().stream()
                .filter(cell -> cell.getCellOwner() == playerType && cell.getBuildings().isEmpty()).collect(Collectors.toList());
    }

    public List<CellStateContainer> getFullCells(PlayerType playerType) {
        return getFlattenedGameMap().stream()
                .filter(cell -> cell.getCellOwner() == playerType && !cell.getBuildings().isEmpty()).collect(Collectors.toList());
    }

    public int numberOfPossibleMoves(PlayerType playerType) {
        int numberOfMovesLeft = 0;
        int energy = getPlayer(playerType).getEnergy();
        long numberOfEmptyCells = getNumberOfEmptyCells(playerType);
        if (energy >= getGameDetails().getBuildingsStats().get(CommandType.BUILD_ENERGY).getPrice()) {
            numberOfMovesLeft += numberOfEmptyCells;
        }
        if (energy >= getGameDetails().getBuildingsStats().get(CommandType.BUILD_DEFENSE).getPrice()) {
            numberOfMovesLeft += numberOfEmptyCells;
        }
        if (energy >= getGameDetails().getBuildingsStats().get(CommandType.BUILD_ATTACK).getPrice()) {
            numberOfMovesLeft += numberOfEmptyCells;
        }
        return numberOfMovesLeft;
    }

    public boolean isGameOver() {
        for (Player player : getPlayers()) {
            if (player.getHealth() <= 0) {
                return true;
            }
        }
        return getGameDetails().getRound() >= MAX_ROUNDS;
    }

    public PlayerType getWinner() {
        Player playerA = getPlayer(PlayerType.A);
        Player playerB = getPlayer(PlayerType.B);
        if (playerA.getHealth() <= 0) {
            return PlayerType.B;
        } else if (playerB.getHealth() <= 0) {
            return PlayerType.A;
        } else if (playerA.getScore() > playerB.getScore()) {
            return PlayerType.A;
        } else if (playerB.getScore() > playerA.getScore()) {
            return PlayerType.B;
        } else {
            return null;
        }
    }

    @Override
    public GameState clone() {
        return new GameState(this);
    }

}
