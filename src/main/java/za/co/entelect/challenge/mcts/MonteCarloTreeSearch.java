package za.co.entelect.challenge.mcts;

import za.co.entelect.challenge.entities.Command;
import za.co.entelect.challenge.entities.GameEngine;
import za.co.entelect.challenge.entities.GameState;
import za.co.entelect.challenge.entities.RandomCommandGenerator;
import za.co.entelect.challenge.enums.PlayerType;

import java.util.ArrayList;
import java.util.Comparator;

public class MonteCarloTreeSearch {

    private static final long MAX_RUN_TIME = 1800000000L;

    private RandomCommandGenerator randomCommandGenerator;
    private GameEngine gameEngine;

    public MonteCarloTreeSearch(RandomCommandGenerator randomCommandGenerator, GameEngine gameEngine) {
        this.randomCommandGenerator = randomCommandGenerator;
        this.gameEngine = gameEngine;
    }

    public Command calculateNextBestMove(GameState gameState) {
        long start = System.nanoTime();
        Node root = new Node(gameState, PlayerType.B);
        while (System.nanoTime() - start < MAX_RUN_TIME) {
            Node interestingNode = selection(root);
            Node newNode = expand(interestingNode);
            boolean won = simulateRandomGame(newNode);
            backPropagate(newNode, won);
        }
        return bestChildBasedOnWinAverage(root);
    }

    private Node selection(Node root) {
        Node node = root;
        while (!node.isTerminal()) {
            if (!node.isFullyExpanded()) {
                return node;
            } else {
                node = bestChildBasedOnUCT(node, root.getNumberOfPlays());
            }
        }
        return node;
    }

    private Node expand(Node node) {
        PlayerType oppositePlayerType = getOpposite(node.getPlayerType());
        Command oppositePlayerCommand = randomCommandGenerator.generate(node.getGameState(), oppositePlayerType, node.getChildren().keySet());
        GameState newGameState = gameEngine.applyCommand(oppositePlayerCommand, node.getGameState());
        Node newNode = new Node(node, newGameState, oppositePlayerType);
        node.addChild(oppositePlayerCommand, newNode);
        if (node.getPlayerType() == PlayerType.B) {
            Command command = randomCommandGenerator.generate(newNode.getGameState(), node.getPlayerType(), new ArrayList<>());
            GameState nextGameState = gameEngine.applyCommand(command, newNode.getGameState());
            Node nextNode = new Node(newNode, nextGameState, node.getPlayerType());
            newNode.addChild(command, nextNode);
            return nextNode;
        } else {
            return newNode;
        }
    }

    private PlayerType getOpposite(PlayerType playerType) {
        if (playerType == PlayerType.A) {
            return PlayerType.B;
        } else {
            return PlayerType.A;
        }
    }

    private boolean simulateRandomGame(Node node) {
        GameState gameState = node.getGameState();
        Command command = randomCommandGenerator.generate(gameState, PlayerType.B, new ArrayList<>());
        gameState = gameEngine.applyCommand(command, gameState);
        return gameEngine.simulateRandomGame(gameState) == PlayerType.A;
    }

    private void backPropagate(Node node, boolean won) {
        node.increaseNumberOfPlays();
        if (won) {
            node.increaseNumberOfWins();
        }
        if (node.hasParent()) {
            backPropagate(node.getParent(), won);
        }
    }

    private Command bestChildBasedOnWinAverage(Node node) {
        return node.getChildren().entrySet().stream().max((o1, o2) -> Float.compare(o1.getValue().getWinAverage(), o2.getValue().getWinAverage())).get().getKey();
    }

    private Node bestChildBasedOnUCT(Node node, int totalNumberOfPlays) {
        return node.getChildren().values().stream().max(Comparator.comparingDouble(o -> uct(o, totalNumberOfPlays))).get();
    }

    private double uct(Node node, int totalNumberOfPlays) {
        return node.getWinAverage() + Math.log(2) * Math.sqrt(Math.log(totalNumberOfPlays) / node.getNumberOfPlays());
    }

}
