package za.co.entelect.challenge.entities;

import za.co.entelect.challenge.enums.PlayerType;

public class GameEngine {

    private RandomCommandGenerator randomCommandGenerator;

    public GameEngine(RandomCommandGenerator randomCommandGenerator) {
        this.randomCommandGenerator = randomCommandGenerator;
    }

    public GameState applyCommand(Command command, GameState gameState) {
        GameState newGameState = gameState.clone();

        Building building = gameState.getGameDetails().getBuildingsStats().get(command.getCommandType());
        newGameState.getGameMap()[command.getX()][command.getY()].addBuilding(building, gameState.getGameDetails().getRound());

        return newGameState;
    }

    public PlayerType simulateRandomGame(GameState gameState) {
        GameState simulateState = gameState.clone();
        while (!gameState.isGameOver()) {
            fireMissiles(gameState);
            moveMissiles(gameState);
            removeDestroyedBuildings(gameState);
            generateEnergy();
        }
        return gameState.getWinner();
    }
}
